package com.example.drivermicroservice;

import java.net.*;  
import java.util.*;
import java.io.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
public class DrivermicroserviceApplication{
   @Autowired
   private KafkaTemplate<String, String> kafkaTemplate;

   public void addDriver() throws IOException {
   	String msg="";
   	  URL url = new URL ("http://localhost:9080/api/drivers");
   	  HttpURLConnection con = (HttpURLConnection)url.openConnection();
   	  con.setRequestMethod("POST");
   	  con.setRequestProperty("Content-Type", "application/json; utf-8");
   	  con.setRequestProperty("Accept", "application/json");
   	  con.setDoOutput(true);
   	  String driverlocation= "{'driverID': 'M3@gmail.com', 'longitude': '-20.3','latitude': '20.3'}";
   	  try(OutputStream os = con.getOutputStream()) {
    byte[] input = driverlocation.getBytes("utf-8");
    os.write(input, 0, input.length);			
}

   	  try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
   	  	StringBuilder response = new StringBuilder();
    String responseLine = null;
    while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
    }
    msg=response.toString();
}

      kafkaTemplate.send("driver_location", msg);
   }
   public static void main(String[] args) {
		SpringApplication.run(DrivermicroserviceApplication.class, args);
	}
   @KafkaListener(topics = "driver_location", groupId = "group-id")
   public void listen(String message) {
      System.out.println("Received Message in group - group-id: " + message);
   }
 
   public void run(ApplicationArguments args) throws Exception {
      addDriver();
   }
}