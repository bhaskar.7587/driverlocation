package com.example.drivermicroservice;
import java.util.HashMap;
import java.util.Map;


import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

@EnableKafka
@Configuration
public class KafkaConsumer {
   @Bean
   public ConsumerFactory<String, String> consumer() {
      Map<String, Object> properties = new HashMap<>();
      properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:2181");
      properties.put(ConsumerConfig.GROUP_ID_CONFIG, "group-id");
      properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
      properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
      return new DefaultKafkaConsumerFactory<>(properties);
   }
   @Bean
   public ConcurrentKafkaListenerContainerFactory<String, String> listener() {
      ConcurrentKafkaListenerContainerFactory<String, String> 
      config = new ConcurrentKafkaListenerContainerFactory<>();
      config.setConsumerFactory(consumer());
      return config;
   }
}