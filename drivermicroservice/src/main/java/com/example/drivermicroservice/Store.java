package com.example.drivermicroservice;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="store")
public class Store{
	@Id
	@GeneratedValue
	private Long storeID;

   private Double latitude;


   private Double longitude;


public Store(){
	super();
}

public Store(Long storeID, Double latitude, Double longitude){
	super();
	this.storeID=storeID;
	this.latitude=latitude;
	this.longitude=longitude;
}

public Long getStoreID(){
	return storeID;
}
public void setStoreID(Long storeID){
	this.storeID=storeID;
}

public Double getLatitude(){
	return latitude;
}
public void setLatitude(Double latitude){
	this.latitude=latitude;
}

public Double getLongitude(){
	return longitude;
}
public void setLongitude(Double longitude){
	this.longitude=longitude;
} 


	

}
