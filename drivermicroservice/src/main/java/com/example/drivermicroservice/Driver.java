package com.example.drivermicroservice;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="driver")
public class Driver{
	@Id
	@GeneratedValue
	private Long id;

   private String driverID;

   private Double latitude;


   private Double longitude;


public Driver(){
	super();
}

public Driver(Long id,String driverID, Double latitude, Double longitude){
	super();
	this.id=id;
	this.driverID=driverID;
	this.latitude=latitude;
	this.longitude=longitude;
}

public String getDriverID(){
	return driverID;
}
public void setDriverID(String driverID){
	this.driverID=driverID;
}

public Double getLatitude(){
	return latitude;
}
public void setLatitude(Double latitude){
	this.latitude=latitude;
}

public Double getLongitude(){
	return longitude;
}
public void setLongitude(Double longitude){
	this.longitude=longitude;
} 


	

}
