package com.example.drivermicroservice;
import com.example.drivermicroservice.Store;
import com.example.drivermicroservice.StoreRepository;
import com.example.drivermicroservice.StoreNotFoundException;

import com.example.drivermicroservice.Driver;
import com.example.drivermicroservice.DriverRepository;
import com.example.drivermicroservice.DriverNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;
import java.util.List;
import java.util.*;
import java.util.stream.LongStream;

@RestController
public class StoreController {
    public static ArrayList<Driver> found=new ArrayList<>();      

@Autowired
    StoreRepository storeRepository;

@Autowired
    static DriverRepository driverRepository;


// Get All stores
    @GetMapping("/api/stores")
    public List<Store> getStores() {
        return storeRepository.findAll();
    }

// Create a new Store
    @PostMapping("/api/stores")
    public Store createStore(@Valid @RequestBody Store store) {
        return storeRepository.save(store);
    }

// Get a Single Store
    @GetMapping("/api/stores/{id}")
    public Store getStoreById(@PathVariable(value = "id") Long storeId) throws StoreNotFoundException {
        return storeRepository.findById(storeId)
                .orElseThrow(() -> new StoreNotFoundException(storeId));
    }

    
    // public static void updateList(Long id) {
    //     Driver results= driverRepository.findById(id);
    //     found.add(results);

    // }

    @GetMapping("/api/stores/{id}/{n}")
    public  ArrayList getStoreById(@PathVariable(value = "id") Long storeId,@PathVariable(value = "n") Long drivers) throws StoreNotFoundException {
        
        Store results= storeRepository.findById(storeId)
                .orElseThrow(() -> new StoreNotFoundException(storeId));

        Double Longitude=results.getLongitude();
        Double Latitude=results.getLatitude();
        
      
        // LongStream.range(1, drivers).forEach(i -> StoreController.updateList(i) ); 
       
        

        return found;

    }

    
// Update a Store
    @PutMapping("/api/stores/{id}")
    public Store updateStores(@PathVariable(value = "id") Long storeId,
                           @Valid @RequestBody Store storeDetails) throws StoreNotFoundException {

Store store = storeRepository.findById(storeId)
                .orElseThrow(() -> new StoreNotFoundException(storeId));

store.setLatitude(storeDetails.getLatitude());
store.setLongitude(storeDetails.getLongitude());

        

Store updatedStore = storeRepository.save(store);

return updatedStore;
    }

// Delete a Store
    @DeleteMapping("/api/stores/{id}")
    public ResponseEntity<?> deleteStore(@PathVariable(value = "id") Long storeId) throws StoreNotFoundException {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(() -> new StoreNotFoundException(storeId));

storeRepository.delete(store);

return ResponseEntity.ok().build();
    }
}