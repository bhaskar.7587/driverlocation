package com.example.drivermicroservice;
public class StoreNotFoundException extends Exception {
private long storeid;
public StoreNotFoundException(long storeid) {
        super(String.format("The store with this id has not been found : '%s'", storeid));
        }
}