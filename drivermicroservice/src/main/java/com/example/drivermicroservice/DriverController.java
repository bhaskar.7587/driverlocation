package com.example.drivermicroservice;
import com.example.drivermicroservice.Driver;
import com.example.drivermicroservice.DriverRepository;
import com.example.drivermicroservice.DriverNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;
import java.util.List;

@RestController
public class DriverController {

@Autowired
    DriverRepository driverRepository;


// Get All Drivers
    @GetMapping("/api/drivers")
    public List<Driver> getDrivers() {
        return driverRepository.findAll();
    }

// Create a new Driver
    @PostMapping("/api/drivers")
    public Driver createDriver(@Valid @RequestBody Driver driver) {
        return driverRepository.save(driver);
    }

// Get a Single Driver
    @GetMapping("/api/drivers/{id}")
    public Driver getDriverById(@PathVariable(value = "id") Long driverId) throws DriverNotFoundException {
        return driverRepository.findById(driverId)
                .orElseThrow(() -> new DriverNotFoundException(driverId));
    }

// Update a Driver
    @PutMapping("/api/drivers/{id}")
    public  Driver updateDrivers(@PathVariable(value = "id") Long driverId,
                           @Valid @RequestBody Driver driverDetails) throws DriverNotFoundException {

Driver driver = driverRepository.findById(driverId)
                .orElseThrow(() -> new DriverNotFoundException(driverId));

driver.setDriverID(driverDetails.getDriverID());
driver.setLatitude(driverDetails.getLatitude());
driver.setLongitude(driverDetails.getLongitude());

        

Driver updatedDriver = driverRepository.save(driver);

return updatedDriver;
    }

// Delete a Driver
    @DeleteMapping("/api/drivers/{id}")
    public ResponseEntity<?> deleteDriver(@PathVariable(value = "id") Long driverId) throws DriverNotFoundException {
        Driver driver= driverRepository.findById(driverId)
                .orElseThrow(() -> new DriverNotFoundException(driverId));

driverRepository.delete(driver);

return ResponseEntity.ok().build();
    }
}