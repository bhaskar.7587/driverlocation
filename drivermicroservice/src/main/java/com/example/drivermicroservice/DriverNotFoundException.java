package com.example.drivermicroservice;
public class DriverNotFoundException extends Exception {
private long storeid;
public DriverNotFoundException(long driverid) {
        super(String.format("The store with this id has not been found : '%s'", driverid));
        }
}